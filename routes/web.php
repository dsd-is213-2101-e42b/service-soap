<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return view('index');
});

$router->get('/test', [
    'as' => 'test',
    'uses' => 'ExampleController@test'
]);

$router->get('/{key}', [
    'as' => 'server.wsdl',
    'uses' => 'SoapController@serverWsdl'
]);

$router->post('/{key}', [
    'as' => 'server',
    'uses' => 'SoapController@server'
]);
