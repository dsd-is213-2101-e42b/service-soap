<!DOCTYPE html>
<html lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>Servicios Soap</title>
    <!-- CSS  -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="{{ url('css/materialize.min.css') }}" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="{{ url('css/prism.css') }}" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="{{ url('css/style.css') }}" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>
<body>
    <header>
        <nav class="top-nav">
            <div class="container">
                <div class="nav-wrapper">
                    <div class="row">
                        <div class="col s12 m10 offset-m1">
                            <h1 class="header">Servicios Soap</h1>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    </header>
    <div class="main">
        <div class="container">
            <div class="section">
                <div class="row">
                    <div class="col s12 l7">
                        <h3 class="header">DNI</h3>
                        <div class="card horizontal">
                            <div class="card-stacked">
                                <div class="card-content">
                                    <p>Sercivio que se encarga de buscar los datos de una persona según su docuemnto de identidad (DNI).</p>
                                    <br>
                                    <p>Contiene el método <code class=" language-markup">search</code>, el cual recibe el parámetro <code class=" language-markup">dni</code></p>
                                </div>
                                <div class="card-action">
                                    <a href="{{ route('server.wsdl', ['key' => 'dni']) }}?wsdl" target="_blank">Ir a wsdl</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12 l7">
                        <h3 class="header">Mail</h3>
                        <div class="card horizontal">
                            <div class="card-stacked">
                                <div class="card-content">
                                    <p>Servicio que se encarga de enviar los emails a los clientes. Se enviará un email al registrarse y al ganar un premio.</p>
                                    <br>
                                    <p>Contiene el método <code class="language-markup">welcome</code>, el cual recibe los parámetros <code class="language-markup">email</code> y <code class="language-markup">name</code></p>
                                    <br>
                                    <p>Contiene el método <code class="language-markup">congrats</code>, el cual recibe los parámetros <code class="language-markup">email</code> y <code class="language-markup">name</code></p>
                                </div>
                                <div class="card-action">
                                    <a href="{{ route('server.wsdl', ['key' => 'mail']) }}?wsdl" target="_blank">Ir a wsdl</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12 l7">
                        <h3 class="header">Establecimientos</h3>
                        <div class="card horizontal">
                            <div class="card-stacked">
                                <div class="card-content">
                                    <p>Servicio que se encarga de crear y buscar establecimientos.</p>
                                    <br>
                                    <p>Contiene el método <code class="language-markup">create</code>, el cual recibe los parámetros <code class="language-markup">name</code>, <code class="language-markup">ruc</code> y <code class="language-markup">email</code></p>
                                    <br>
                                    <p>Contiene el método <code class="language-markup">search</code>, el cual recibe el parámetro <code class="language-markup">query</code></p>
                                </div>
                                <div class="card-action">
                                    <a href="{{ route('server.wsdl', ['key' => 'establishments']) }}?wsdl" target="_blank">Ir a wsdl</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12 l7">
                        <h3 class="header">Cupones</h3>
                        <div class="card horizontal">
                            <div class="card-stacked">
                                <div class="card-content">
                                    <p>Servicio que se encarga administrar y usar los cupones.</p>
                                    <br>
                                    <p>Contiene el método <code class="language-markup">create</code>, el cual recibe el parámetro <code class="language-markup">establishment</code></p>
                                    <br>
                                    <p>Contiene el método <code class="language-markup">search</code>, el cual recibe el parámetro <code class="language-markup">code</code></p>
                                    <br>
                                    <p>Contiene el método <code class="language-markup">use</code>, el cual recibe el parámetro <code class="language-markup">code</code></p>
                                </div>
                                <div class="card-action">
                                    <a href="{{ route('server.wsdl', ['key' => 'coupons']) }}?wsdl" target="_blank">Ir a wsdl</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer class="page-footer docs-footer">
        <div class="container">
            <div class="row" style="margin-bottom: 0;">
                <div class="col s12 m10 offset-m1">
                    <div class="footer-copyright">
                        © 2014-2021 Prizes, All rights reserved.
                        <span class="grey-text text-darken-1 right">MIT License</span>
                    </div>

                </div>
            </div>
        </div>
    </footer>

    <!--  Scripts-->
    <script src="{{ url('js/jquery-2.1.1.min.js') }}"></script>
    <script src="{{ url('js/materialize.min.js') }}"></script>

</body>
</html>
