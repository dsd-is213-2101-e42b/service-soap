<?php

return [
    'sendgrid_api_key' => env('MAIL_SENDGRID_API_KEY', ''),
    'from_email' => env('MAIL_FROM_EMAIL', ''),
    'from_name' => env('app_name', ''),
];
