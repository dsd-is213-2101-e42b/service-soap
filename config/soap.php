<?php

return [
    'services' => [
        'dni' => [
            'name' => 'ServicioDNI',
            'class' => App\Soap\Services\PersonService::class,
            'exceptions' => [
                'Exception'
            ],
            'types' => [],
            'strategy' => 'ArrayOfTypeComplex',
            'headers' => [
                'Cache-Control' => 'no-cache, no-store'
            ],
            'options' => [],
        ],
        'mail' => [
            'name' => 'ServicioMail',
            'class' => App\Soap\Services\MailService::class,
            'exceptions' => [
                'Exception'
            ],
            'types' => [],
            'strategy' => 'ArrayOfTypeComplex',
            'headers' => [
                'Cache-Control' => 'no-cache, no-store'
            ],
            'options' => [],
        ],
        'establishments' => [
            'name' => 'ServicioEstablecimientios',
            'class' => App\Soap\Services\EstablishmentService::class,
            'exceptions' => [
                'Exception'
            ],
            'types' => [],
            'strategy' => 'ArrayOfTypeComplex',
            'headers' => [
                'Cache-Control' => 'no-cache, no-store'
            ],
            'options' => [],
        ],
        'coupons' => [
            'name' => 'ServicioCupones',
            'class' => App\Soap\Services\CouponsService::class,
            'exceptions' => [
                'Exception'
            ],
            'types' => [],
            'strategy' => 'ArrayOfTypeComplex',
            'headers' => [
                'Cache-Control' => 'no-cache, no-store'
            ],
            'options' => [],
        ],
    ],

    'logging' => true,
];
