<?php

namespace Database\Factories;

use App\Models\Person;
use Illuminate\Database\Eloquent\Factories\Factory;

class PersonFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Person::class;

    const CITIES = [
        'Amazonas',
        'Áncash',
        'Apurímac',
        'Arequipa',
        'Ayacucho',
        'Cajamarca',
        'Callao',
        'Cusco',
        'Huancavelica',
        'Huánuco',
        'Ica',
        'Junín',
        'La Libertad',
        'Lambayeque',
        'Lima',
        'Loreto',
        'Madre de Dios',
        'Moquegua',
        'Pasco',
        'Piura',
        'Puno',
        'San Martín',
        'Tacna',
        'Tumbes',
        'Ucayali',
    ];

    const MARITAL_STATUSES = [
        'male' => [
            'Soltero', 'Casado', 'Divorciado', 'Viudo',
        ],
        'female' => [
            'Soltera', 'Casada', 'Divorciada', 'Viuda',
        ]
    ];

    const GENDERS = [
        'male', 'female'
    ];

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        $gender = $this->faker->randomElement(self::GENDERS);

        return [
            'name' => $this->faker->firstName($gender),
            'lastname1' => $this->faker->lastName,
            'lastname2' => $this->faker->lastName,
            'birthdate' => $this->faker->date,
            'city' => $this->faker->randomElement(self::CITIES),
            'marital_status' => $this->faker->randomElement(self::MARITAL_STATUSES[$gender]),
            'address' => $this->faker->streetAddress,
            'gender' => mb_strtoupper(substr($gender, 0, 1)),
        ];
    }
}
