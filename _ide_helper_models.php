<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\Coupon
 *
 * @property int $id
 * @property string $code
 * @property int $winner
 * @property string|null $used
 * @property int $establishment_id
 * @property-read Coupon $establishment
 * @method static \Illuminate\Database\Eloquent\Builder|Coupon newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Coupon newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Coupon query()
 * @method static \Illuminate\Database\Eloquent\Builder|Coupon whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Coupon whereEstablishmentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Coupon whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Coupon whereUsed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Coupon whereWinner($value)
 */
	class Coupon extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Establishment
 *
 * @property int $id
 * @property string $name
 * @property string $ruc
 * @property string $email
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Coupon[] $coupons
 * @property-read int|null $coupons_count
 * @method static \Illuminate\Database\Eloquent\Builder|Establishment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Establishment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Establishment query()
 * @method static \Illuminate\Database\Eloquent\Builder|Establishment whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Establishment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Establishment whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Establishment whereRuc($value)
 */
	class Establishment extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Person
 *
 * @method static \Database\Factories\PersonFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Person newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Person newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Person query()
 */
	class Person extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\User
 *
 * @method static \Database\Factories\UserFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 */
	class User extends \Eloquent implements \Illuminate\Contracts\Auth\Authenticatable, \Illuminate\Contracts\Auth\Access\Authorizable {}
}

