<?php

function generateRandomCode(int $length = 4): string
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $length_characters = strlen($characters);
    $code = '';
    for ($i = 0; $i < $length; $i++) {
        $code .= $characters[random_int(0, $length_characters - 1)];
    }

    return $code;
}

function couponWin(): bool
{
    return mt_rand(1, 10) > 7;
}
