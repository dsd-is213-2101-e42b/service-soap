<?php

namespace App\Classes;

use Exception;
use SendGrid;
use SendGrid\Mail\From;
use SendGrid\Mail\HtmlContent;
use SendGrid\Mail\Mail as Email;
use SendGrid\Mail\Subject;
use SendGrid\Mail\To;

class Mail
{
    protected $email;

    protected $from;

    protected $to;

    protected $subject;

    protected $htmlContent;

    protected $template;

    protected $data;

    public function __construct()
    {
        $this->loadFrom();
    }

    /**
     * @return void
     * @noinspection PhpDocMissingThrowsInspection
     */
    protected function loadMail(): void
    {
        $mail =  new Email();
        /** @noinspection PhpUnhandledExceptionInspection */
        $mail->setFrom($this->from);
        /** @noinspection PhpUnhandledExceptionInspection */
        $mail->addTo($this->to);
        /** @noinspection PhpUnhandledExceptionInspection */
        $mail->setSubject($this->subject);
        /** @noinspection PhpUnhandledExceptionInspection */
        $mail->addContent($this->htmlContent);
        $this->email = $mail;
    }

    /**
     * @return void
     * @noinspection PhpDocMissingThrowsInspection
     */
    protected function loadFrom(): void
    {
        $email = config('mail.from_email');
        $name = config('mail.from_name');
        /** @noinspection PhpUnhandledExceptionInspection */
        $this->from = new From($email, $name);
    }

    /**
     * @return bool
     */
    protected function canSendEmail(): bool
    {
        return $this->from and $this->to and $this->subject and $this->htmlContent;
    }

    /**
     * @return SendGrid|null
     */
    protected function getSendGrid(): ?SendGrid
    {
        $api_key = config('mail.sendgrid_api_key', '');

        if ($api_key === '') {
            return null;
        }

        return new SendGrid($api_key);
    }

    /**
     * @param string $email
     * @param string $name
     * @return void
     * @noinspection PhpDocMissingThrowsInspection
     */
    public function setTo(string $email, string $name = ''): void
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $this->to = new To($email, $name);
    }

    /**
     * @param string $subject
     * @return void
     * @noinspection PhpDocMissingThrowsInspection
     */
    public function setSubject(string $subject): void
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $this->subject = new Subject($subject);
    }

    /**
     * @param string $template
     * @param array $data
     * @return void
     * @noinspection PhpDocMissingThrowsInspection
     */
    public function setContent(string $template, array $data): void
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $this->htmlContent = new HtmlContent(view($template, $data)->render());
    }

    /**
     * @return bool
     */
    public function send(): bool
    {
        $this->loadMail();
        if (!$this->canSendEmail()) {
            return false;
        }

        $sendgrid = $this->getSendGrid();
        if (is_null($sendgrid)) {
            return false;
        }

        try {
            $sendgrid = $this->getSendGrid();
            $response = $sendgrid->send($this->email);
            return $response->statusCode() === 202;
        } catch (Exception $exception) {
            return false;
        }
    }
}
