<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code', 'winner', 'used', 'establishment_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'id', 'winner'
    ];

    public $timestamps = false;

    public function establishment()
    {
        return $this->belongsTo(Coupon::class);
    }

    public static function generateCode(): string
    {
        $code = generateRandomCode(4);
        if (!is_null(Coupon::where('code', '=', $code)->first())) {
            self::generateCode();
        }

        return $code;
    }
}
