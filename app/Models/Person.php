<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'dni', 'name', 'lastname1', 'lastname2', 'birthdate', 'city', 'gender', 'status'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'id',
    ];

    public $timestamps = false;

    protected $table = 'persons';

    /**
     * @param $dni
     * @return array
     */
    public static function search($dni): array
    {
        if (!(new Person)->isDniValid($dni)) {
            return [];
        }

        $person = Person::where(['dni' => $dni])->first();
        if (!$person) {
            return (new Person)->create($dni);
        }

        return $person->toArray();
    }

    /**
     * @param $dni
     * @return bool
     */
    public function isDniValid($dni): bool
    {
        $dni = (int)$dni;
        return strlen($dni) === 8;
    }

    /**
     * @param $dni
     * @return array
     */
    public function create($dni): array
    {
        if (!self::isDniValid($dni)) {
            return [];
        }

        return self::factory(Person::class)->create([
            'dni' => $dni
        ])->toArray();
    }
}
