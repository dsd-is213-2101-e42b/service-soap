<?php

namespace App\Soap\Services;

use App\Classes\Mail;

class MailService
{
    /**
     *
     * Send a welcome email
     *
     * @param string $email
     * @param string $name
     * @return bool
     */
    public function sendWelcomeMail(string $email, string $name): bool
    {
        $mail = new Mail();
        $mail->setTo($email, $name);
        $mail->setSubject('Bienvenido');
        $mail->setContent('mail.welcome', compact('email', 'name'));
        return $mail->send();
    }

    /**
     *
     * Send a congrats email
     *
     * @param string $email
     * @param string $name
     * @return bool
     */
    public function sendCongratsEmail(string $email, string $name): bool
    {
        $mail = new Mail();
        $mail->setTo($email, $name);
        $mail->setSubject('¡Felicitaciones!');
        $mail->setContent('mail.congrats', compact('email', 'name'));
        return $mail->send();
    }
}
