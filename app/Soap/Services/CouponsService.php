<?php

namespace App\Soap\Services;

use App\Models\Coupon;
use DateTime;
use DB;
use Exception;
use SoapFault;

class CouponsService
{
    /**
     * Create an coupon
     *
     * @param int $establishment
     * @return string
     * @throws SoapFault
     */
    public function create(int $establishment): string
    {
        try {
            $coupon = new Coupon;
            $coupon->code = Coupon::generateCode();
            $coupon->winner = false;
            $coupon->used = null;
            $coupon->establishment_id = $establishment;
            $coupon->save();
            return $coupon->code;
        } catch (Exception $exception) {
            throw new SoapFault('SOAP-ENV:Client', 'Se produjo un error');
        }
    }

    /**
     * Search a coupon by code
     *
     * @param string $code
     * @return bool
     * @throws SoapFault
     */
    public function search(string $code): bool
    {
        $show_message = false;

        if (mb_strlen($code) !== 4) {
            throw new SoapFault('SOAP-ENV:Client', 'El código debe tener 4 carácteres alfanuméricos.');
        }

        try {
            $coupon = Coupon::where(DB::raw('BINARY code'), '=', $code)
                ->first();

            if (is_null($coupon)) {
                $show_message = true;
                throw new SoapFault('SOAP-ENV:Client', 'Cupón inválido');
            }

            if ($coupon->used) {
                $show_message = true;
                throw new SoapFault('SOAP-ENV:Client', 'El cupón ya fue usado');
            }

            return !is_null($coupon);
        } catch (Exception $exception) {
            throw new SoapFault('SOAP-ENV:Client', $show_message ? $exception->getMessage() : 'Se produjo un error');
        }
    }

    /**
     * Use coupon
     *
     * @param string $code
     * @return bool
     * @throws SoapFault
     */
    public function use(string $code): bool
    {
        $show_message = false;

        try {
            if (mb_strlen($code) !== 4) {
                $show_message = true;
                throw new SoapFault('SOAP-ENV:Client', 'El código debe tener 4 carácteres alfanuméricos.');
            }

            $coupon = Coupon::where(DB::raw('BINARY code'), '=', $code)
                ->first();

            if (is_null($coupon)) {
                $show_message = true;
                throw new SoapFault('SOAP-ENV:Client', 'Cupón inválido');
            }

            if ($coupon->used) {
                $show_message = true;
                throw new SoapFault('SOAP-ENV:Client', 'El cupón ya fue usado');
            }

            $coupon->winner = couponWin();
            $coupon->used = new DateTime();
            $coupon->save();
            return $coupon->winner;
        } catch (Exception $exception) {
            throw new SoapFault('SOAP-ENV:Client', $show_message ? $exception->getMessage() : 'Se produjo un error');
        }
    }
}
