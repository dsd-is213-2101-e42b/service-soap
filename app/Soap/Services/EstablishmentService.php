<?php

namespace App\Soap\Services;

use App\Models\Establishment;
use Exception;
use SoapFault;

class EstablishmentService
{
    /**
     * Create an establishments
     *
     * @param string $name
     * @param string $ruc
     * @param string $email
     * @return bool
     * @throws SoapFault
     */
    public function create(string $name, string $ruc, string $email): bool
    {
        try {
            $establishments = new Establishment;
            $establishments->name = $name;
            $establishments->ruc = $ruc;
            $establishments->email = $email;
            $establishments->save();
            return true;
        } catch (Exception $exception) {
            throw new SoapFault('SOAP-ENV:Client', 'Se produjo un error');
        }
    }

    /**
     * Returns an establishments by name or ruc
     *
     * @param string $query
     * @return array
     * @throws SoapFault
     */
    public function search(string $query): array
    {
        try {
            $establishments = Establishment::where('name', 'like', '%' . $query . '%')
                ->orWhere('ruc', 'like', '%' . $query . '%')
                ->get();
            return $establishments->toArray();
        } catch (Exception $exception) {
            throw new SoapFault('SOAP-ENV:Client', 'Se produjo un error');
        }
    }
}
