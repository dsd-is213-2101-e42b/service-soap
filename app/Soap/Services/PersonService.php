<?php

namespace App\Soap\Services;

use App\Models\Person;
use Exception;
use SoapFault;

class PersonService
{
    /**
     *
     * Returns a person by dni.
     *
     * @param string $dni
     * @return array
     * @throws SoapFault
     */
    public function search(string $dni): array
    {
        $show_message = false;

        try {
            $person = Person::search($dni);

            if (count($person) < 1) {
                $show_message = true;
                throw new SoapFault('SOAP-ENV:Client', 'No se encontraron resultados.');
            }

            return $person;
        } catch (Exception $exception) {
            throw new SoapFault('SOAP-ENV:Client', $show_message ? $exception->getMessage() : 'Se produjo un error');
        }
    }
}
