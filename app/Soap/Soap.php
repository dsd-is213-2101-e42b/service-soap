<?php

namespace App\Soap;

use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Illuminate\View\View;
use Laminas\Soap\Wsdl\ComplexTypeStrategy\ComplexTypeStrategyInterface;

class Soap
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $service;

    /**
     * @var string
     */
    protected $endpoint;

    /**
     * @var array
     */
    protected $exceptions;

    /**
     * @var array
     */
    protected $types;

    /**
     * @var ComplexTypeStrategyInterface
     */
    protected $strategy;

    /**
     * @var array
     */
    protected $headers;

    /**
     * @var array
     */
    protected $options;


    /**
     * Soap constructor.
     * @param string $key
     * @throws Exception
     */
    public function __construct(string $key)
    {
        try {
            $this->init($key);
        } catch (Exception $e) {

        }
    }


    /**
     * @param string $key
     * @throws Exception
     */
    public function init(string $key)
    {
        $config = config('soap.services.' . $key);

        if (!$config) {
            throw new Exception('Configuración de servicio inválida');
        }

        $this->name = $config['name'];
        $this->service = $config['class'];
        $this->endpoint = self::currentUrlRoot();
        $this->exceptions = $config['exceptions'];
        $this->types = $config['types'];

        $strategies = [
            'AnyType' => \Laminas\Soap\Wsdl\ComplexTypeStrategy\AnyType::class,
            'ArrayOfTypeComplex' => \Laminas\Soap\Wsdl\ComplexTypeStrategy\ArrayOfTypeComplex::class,
            'ArrayOfTypeSequence'=> \Laminas\Soap\Wsdl\ComplexTypeStrategy\ArrayOfTypeSequence::class,
            'DefaultComplexType' => \Laminas\Soap\Wsdl\ComplexTypeStrategy\DefaultComplexType::class,
        ];

        $strategy = ($config['strategy']) ? : 'ArrayOfTypeComplex';

        if (!array_key_exists($strategy, $strategies)) {
            throw new Exception('Especifique un complex type strategy válido.');
        }

        $strategy = $strategies[$strategy];
        $this->strategy = new $strategy();

        $this->headers = $config['headers'];
        $this->options = array_key_exists('options', $config) ? $config['options'] : [];

        if (!array_key_exists('Content-Type', $this->headers)) {
            $this->headers = Arr::add($this->headers, 'Content-Type', 'application/xml; charset=utf-8');
        }

        ini_set('soap.wsdl_cache_enable', 0);
        ini_set('soap.wsdl_cache_ttl', 0);
    }

    /**
     * Get the current absolute URL path, minus the query string.
     *
     * @return string
     */
    public static function currentUrlRoot(): string
    {
        $url = url(app()->request->server()['REQUEST_URI']);
        $pos = strpos($url, '?');
        return $pos ? substr($url, 0, $pos) : $url;
    }

    /**
     * Log message if logging is enabled in config, return input fluently.
     *
     * @param string $message
     * @return string
     */
    public static function log(string $message = ''): string
    {
        if (config('zoap.logging', false)) {
            Log::info($message);
        }

        return $message;
    }

    /**
     * Return error response and log stack trace.
     *
     * @param Exception $exception
     * @return View
     */
    public static function serverFault(Exception $exception): View
    {
        self::log($exception->getTraceAsString());
        $faultcode = 'SOAP-ENV:Server';
        $faultstring = $exception->getMessage();
        return view('soap.fault', compact('faultcode', 'faultstring'));
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getService(): string
    {
        return $this->service;
    }

    /**
     * @param string $service
     */
    public function setService(string $service): void
    {
        $this->service = $service;
    }

    /**
     * @return string
     */
    public function getEndpoint(): string
    {
        return $this->endpoint;
    }

    /**
     * @param string $endpoint
     */
    public function setEndpoint(string $endpoint): void
    {
        $this->endpoint = $endpoint;
    }

    /**
     * @return array
     */
    public function getExceptions(): array
    {
        return $this->exceptions;
    }

    /**
     * @param array $exceptions
     */
    public function setExceptions(array $exceptions): void
    {
        $this->exceptions = $exceptions;
    }

    /**
     * @return array
     */
    public function getTypes(): array
    {
        return $this->types;
    }

    /**
     * @param array $types
     */
    public function setTypes(array $types): void
    {
        $this->types = $types;
    }

    /**
     * @return ComplexTypeStrategyInterface
     */
    public function getStrategy(): ComplexTypeStrategyInterface
    {
        return $this->strategy;
    }

    /**
     * @param ComplexTypeStrategyInterface $strategy
     */
    public function setStrategy(ComplexTypeStrategyInterface $strategy): void
    {
        $this->strategy = $strategy;
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * @param array $headers
     */
    public function setHeaders(array $headers): void
    {
        $this->headers = $headers;
    }

    /**
     * @return array
     */
    public function getOptions(): array
    {
        return $this->options;
    }

    /**
     * @param array $options
     */
    public function setOptions(array $options): void
    {
        $this->options = $options;
    }
}
