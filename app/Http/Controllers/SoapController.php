<?php

namespace App\Http\Controllers;

use App\Soap\Soap;
use Exception;
use Illuminate\Http\Response;
use Laminas\Soap\AutoDiscover;
use Laminas\Soap\Server;
use Laminas\Soap\Server\DocumentLiteralWrapper;
use Laminas\Soap\Wsdl;
use SoapFault;

class SoapController extends Controller
{
    /**
     * @param string $key
     * @return Response
     */
    public function serverWsdl(string $key): Response
    {
        $output = new Response();

        if (array_key_exists('wsdl', request()->all())) {
            ob_start();
            try {
                $soap = new Soap($key);

                foreach ($soap->getHeaders() as $index => $header) {
                    $output->headers->set($index, $header);
                }

                $wsdl = new Wsdl('wsdl', $soap->getEndpoint());
                foreach ($soap->getTypes() as $index => $type) {
                    $wsdl->addType($type, $index);
                }

                $soap->getStrategy()->setContext($wsdl);

                foreach ($soap->getTypes() as $type) {
                    $soap->getStrategy()->addComplexType($type);
                }

                $discover = new AutoDiscover($soap->getStrategy());
                $discover->setBindingStyle(['style' => 'document']);
                $discover->setOperationBodyStyle(['use' => 'literal']);
                $discover->setClass($soap->getService());
                $discover->setUri($soap->getEndpoint());
                $discover->setServiceName($soap->getName());
                echo $discover->toXml();
            } catch (Exception $exception) {
                $output->headers->set('Status', 500);
                echo Soap::serverFault($exception);
            }

            $output->setContent(ob_get_clean());
        }

        return $output;
    }

    /**
     * @param string $key
     * @return Response
     */
    public function server(string $key): Response
    {
        $output = new Response();
        ob_start();

        try {
            $soap = new Soap($key);

            foreach ($soap->getHeaders() as $index => $header) {
                $output->headers->set($index, $header);
            }

            $service = $soap->getService();

            $server = new Server($soap->getEndpoint() . '?wsdl');
            $server->setClass(new DocumentLiteralWrapper(new $service()));
            $server->registerFaultException($soap->getExceptions());
            $server->setOptions($soap->getOptions());

            $server->setReturnResponse(true);
            $response = $server->handle();

            if ($response instanceof SoapFault) {
                $output->headers->set('Status', 500);
                echo Soap::serverFault($response);
            } else {
                echo $response;
            }
        } catch (Exception $exception) {
            $output->headers->set('Status', 500);
            echo Soap::serverFault($exception);
        }

        $output->setContent(ob_get_clean());
        return $output;
    }
}
